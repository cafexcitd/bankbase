class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy]
  before_filter :authorize_admin, only: :index

  require 'json'

  # GET /accounts
  # GET /accounts.json
  def index
    @accounts = Account.all
    @loan = Loan.new
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show

    @loan = Loan.new

    @activity = []

    @account.deposits.each do |deposit|
      @activity << deposit
    end 

    @account.withdrawals.each do |withdrawal|
      @activity << withdrawal
    end 

    @activity = @activity.sort_by &:created_at

    @values = []

    @activity.each do |action|

      if action.class ==  Deposit
      @values << action.amount
      else 
      @values << -action.amount
      end

    end   

    s = 0
    @values = @values.map{|e| s += e}

    @dates = []

    @activity.each do |action|
      @dates << action.created_at.to_formatted_s(:short).to_s
    end      

    @dates = @dates


  end

  # GET /accounts/new
  def new
    @account = Account.new
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new(account_params)

    respond_to do |format|
      if @account.save
        format.html { redirect_to @account, notice: 'Account was successfully created.' }
        format.json { render :show, status: :created, location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to @account, notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end

    def authorize_admin
      flash[:alert] = "You are not cool enough to do this - go back from whence you came."
      redirect_to root_path unless current_user.admin?
    #redirects to previous page
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:user_id, :number, :name)
    end
end
