class TeamsController < ApplicationController
  before_action :set_team, only: [:show, :edit, :update, :token, :destroy, :console, :shortcode,
    :example1, :example2, :example3, :example4, :example5, :palettes]
  skip_before_filter :verify_authenticity_token
  before_action :authenticate_user!

  before_filter :admin_user,  only: [:create, :index]


  require "json"
  require "uri"
  require "net/http"
  require "net/https"
  require "open-uri"
  require "execjs"

  # GET /teams
  # GET /teams.json
  def index

    @teams = Team.all
    authorize @teams
  end

  def home
    redirect_to team_path(id: current_user.team.id)
  end

  # GET /teams/1
  # GET /teams/1.json
  def show  
     authorize @team
     @json = JSON.pretty_generate(@team.sessionDescription(@team.correlation_id))

  end

  def palettes

    @lang = params[:lang]
    @csdk = params[:csdk]

    @get_url = URI("http://#{@team.server}/palettes_support/service?applicationId=uccxadapter&protocolVersion=2")
    @get_request = Net::HTTP::Get.new(@get_url.path, { "Content-Type" => "application.json"})
    @get_httpClient = Net::HTTP.new(@get_url.host, @get_url.port).start()
    @get_response = @get_httpClient.request(@get_request)

    @link = JSON.parse(@get_response.body).values[1][0]["_link"]
    @type = JSON.parse(@get_response.body).values[1][0]["type"]

    # Palettes Post
  
    @hash = {}

    @hash["context"] = {lang: @lang, accno: current_user.accounts.first.number.to_s, csdkCall: @csdk, customerName: current_user.name, email: current_user.email, caller: current_user.id  }
    @hash["key"] = current_user.id

    @url = URI("http://#{@team.server}#{@link}/method/retrieveData")

    @request = Net::HTTP::Post.new(@url.path, { "Content-Type" => "application.json", "Content-Length" => @hash.length().to_s() })
    @request.body = @hash.to_json()

    @httpClient = Net::HTTP.new(@url.host, @url.port).start()
    @response = @httpClient.request(@request)

    @response = @response
    @body = @response.body
    @header = @response.header
    @code = @response.code

    respond_to do |format|
      format.html
      format.json
    end

  end

  def example1

  end

  def example2


  end


  def shortcode

    @url = "http://#{@team.server}/assistserver/shortcode/create"

    @put_url = URI(@url)
    @put_request = Net::HTTP::Put.new(@put_url.path)
    @put_httpClient = Net::HTTP.new(@put_url.host, @put_url.port).start()
    @put_response = @put_httpClient.request(@put_request).body

    @appkey = JSON.parse(@put_response).to_h["shortCode"].to_s


    @get_url = URI("http://#{@team.server}/assistserver/shortcode/consumer?appkey=#{@appkey}")

    @get_request = Net::HTTP::Get.new(@get_url)
    @get_httpClient = Net::HTTP.new(@get_url.host, @get_url.port).start()
    
    @get_response = @get_httpClient.request(@get_request)

    until @get_response.code == "200"  do
      @get_response = @get_httpClient.request(@get_request)
    end

    @response = {}
    @response[:get_response] = JSON.parse(@get_response.body)
    @response[:shortCode] = @appkey

    @response = @response.to_json



   render :layout => 'plain'  

  end


  def example3

  end

  def example4

    hash = @team.sessionDescription(@team.correlation_id)

    url = URI("http://#{@team.server}/gateway/sessions/session")

    request = Net::HTTP::Post.new(url.path, { "Content-Type" => "application.json", "Content-Length" => hash.length().to_s() })
    request.body = hash.to_json()

    httpClient = Net::HTTP.new(url.host, url.port).start()
    response = httpClient.request(request)

    @token = JSON.parse(response.body)["sessionid"]
  end

  def example5

   
    hash = @team.sessionDescription(@team.correlation_id)

    url = URI("http://#{@team.server}/gateway/sessions/session")

    request = Net::HTTP::Post.new(url.path, { "Content-Type" => "application.json", "Content-Length" => hash.length().to_s() })
    request.body = hash.to_json()

    httpClient = Net::HTTP.new(url.host, url.port).start()
    response = httpClient.request(request)

    @token = JSON.parse(response.body)["sessionid"]


  end

  def console

   
    @hash = @team.agentDescription


    @url = URI("http://#{@team.server}/gateway/sessions/session")

    request = Net::HTTP::Post.new(@url.path, { "Content-Type" => "application.json", "Content-Length" => @hash.length().to_s() })
    request.body = @hash.to_json()

    httpClient = Net::HTTP.new(@url.host, @url.port).start()
    response = httpClient.request(request)

    @token = JSON.parse(response.body)["sessionid"]


  end
  

  # GET /teams/new
  def new
    @team = Team.new
  end

  # GET /teams/1/edit
  def edit
    authorize @team
  end

  # POST /teams
  # POST /teams.json
  def create
    @team = Team.new(team_params)

    respond_to do |format|
      if @team.save
        format.html { redirect_to @team, notice: 'Team was successfully created.' }
        format.json { render :show, status: :created, location: @team }
      else
        format.html { render :new }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /teams/1
  # PATCH/PUT /teams/1.json
  def update
    respond_to do |format|
      if @team.update(team_params)
        format.html { redirect_to @team, notice: 'Team was successfully updated.' }
        format.json { render :show, status: :ok, location: @team }
      else
        format.html { render :edit }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teams/1
  # DELETE /teams/1.json
  def destroy
    @team.destroy
    respond_to do |format|
      format.html { redirect_to teams_url, notice: 'Team was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

    def user_not_authorized
      flash[:alert] = "You are not cool enough to do this - go back from whence you came."
      redirect_to(root_path)
    end

 
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.require(:team).permit(:name, :webappid, :server, :number, :agent, :correlation_id, :fqdn, :info, :uui_data, :allowed_origins)
    end
end
