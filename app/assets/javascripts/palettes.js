 Palettes.setVerbose(true);
    var palettes;

    var server = "<%= @team.fqdn %>";


    function handleError(msg, e)
    {        
        if (e != undefined) msg = msg + " exception " + JSON.stringify(e);
        alert("error " + msg);
    }
    
    function handleResult() {
        alert("posted");
    }

    function submitData(lang) {
        if (palettes == undefined) {
            alert("Palettes not started");
            return;
        }
        

        Palettes.log("POST :");

        var serviceParms = {};
        serviceParms["context"] = {language: lang, accno: "<%= current_user.accounts.first.number %>", customerName: "<%= current_user.name %>", caller: "<%= current_user.id %>",  };
        serviceParms["key"] = "<%= current_user.accounts.first.number %>";
        console.log("params: "+ serviceParms);

        palettes.invoke( "data", "retrieveData", serviceParms, handleResult, handleError );
        

    }
    
    function handleStart(palettesObj) {
        palettes = palettesObj;
        // $("#submit").prop( "disabled", false );

    }

    function start() {
        Palettes.init("https://"+server+":8443", "uccxadapter", handleStart, handleError); // initialise palettes
    }

	start();