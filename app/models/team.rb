class Team < ActiveRecord::Base

belongs_to :user


def sessionDescription(user)

	 sessionDescription = {
            "webAppId" => self.webappid || "",
            "allowedOrigins" => [self.allowed_origins || "*"],
            "urlSchemeDetails" =>
                      {
                          "secure" => true,
                          "host" => self.fqdn || "",
                          "port" => "443"
            },
            "voice" => {
                "username" => user || "",
                "domain" => self.fqdn || ""
            },
            "additionalAttributes" => {
                "AED2.metadata" => {
                    "role" => "consumer"
                },
                "AED2.allowedTopic" => self.correlation_id || ""
            },
            "uuiData" => self.uui_data
      }

      return sessionDescription

end

def agentDescription

   agentDescription = {
            "webAppId" => self.webappid || "",
            "allowedOrigins" =>[self.allowed_origins || "*"],
            "urlSchemeDetails" =>
                      {
                          "secure" => true,
                          "host" => self.fqdn || "",
                          "port" => "443"
            },
            "voice" => {
                "username" => self.agent || "",
                "domain" => self.fqdn || "",
                "inboundCallingEnabled" => true
            },
            "additionalAttributes" => {
                "AED2.metadata" => {
                    "role" => "agent",
                    "name" => self.agent || ""
              },
            "AED2.allowedTopic" => ".*"
            },
            "uuiData" => self.uui_data
      }

      return agentDescription

end


end
