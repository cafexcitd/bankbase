class Account < ActiveRecord::Base

belongs_to :user

has_many :deposits

has_many :withdrawals

def balance

	@balance = 0

	self.deposits.each do |deposit|
		@balance = @balance + deposit.amount
	end

	self.withdrawals.each do |withdrawal|
		@balance = @balance - withdrawal.amount
	end

	return @balance

end

	def activity

	    @activity = []

	    self.deposits.each do |deposit|
	      @activity << deposit
	    end 

	    self.withdrawals.each do |withdrawal|
	      @activity << withdrawal
	    end 

	    @activity = @activity.sort_by &:created_at

	

	    return @activity

	end

	def values

		@values = []

	    self.activity.each do |action|

	      if action.class ==  Deposit
	      @values << action.amount
	      else 
	      @values << -action.amount
	      end

	    end   

		    s = 0
    		@values = @values.map{|e| s += e}

    		return @values
	end

    def dates

	    @dates = []

	    self.activity.each do |action|
	      @dates << action.created_at.to_formatted_s(:short).to_s
	    end      

	    @dates = @dates

	    return @dates

   end


end
