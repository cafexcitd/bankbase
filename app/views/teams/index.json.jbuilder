json.array!(@teams) do |team|
  json.extract! team, :id, :name, :webappid, :server
  json.url team_url(team, format: :json)
end
