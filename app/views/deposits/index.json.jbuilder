json.array!(@deposits) do |deposit|
  json.extract! deposit, :id, :user_id, :account_id, :amount
  json.url deposit_url(deposit, format: :json)
end
