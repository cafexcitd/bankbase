json.array!(@withdrawals) do |withdrawal|
  json.extract! withdrawal, :id, :user_id, :account_id, :amount
  json.url withdrawal_url(withdrawal, format: :json)
end
