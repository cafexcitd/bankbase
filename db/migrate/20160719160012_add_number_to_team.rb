class AddNumberToTeam < ActiveRecord::Migration
  def change
    add_column :teams, :number, :string
    add_column :teams, :agent, :string
    add_column :teams, :correlation_id, :string
  end
end
