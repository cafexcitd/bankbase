class AddAllowedOriginsToTeam < ActiveRecord::Migration
  def change
    add_column :teams, :allowed_origins, :string
  end
end
