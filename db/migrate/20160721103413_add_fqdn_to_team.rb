class AddFqdnToTeam < ActiveRecord::Migration
  def change
    add_column :teams, :fqdn, :string
  end
end
