class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.integer :user_id
      t.float :amount
      t.float :rate
      t.integer :term
      t.integer :repayment

      t.timestamps null: false
    end
  end
end
