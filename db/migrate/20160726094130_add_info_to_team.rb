class AddInfoToTeam < ActiveRecord::Migration
  def change
    add_column :teams, :info, :text
  end
end
