Rails.application.routes.draw do

  scope "/deploymentdemo/" do
      resources :loans
      resources :teams

      get "about", to: "pages#about"
      post "token/:id" => "visitors#token", as: :token
      post "teams/:id/token" => "teams#token", as: :team_token

      resources :withdrawals
      resources :deposits
      resources :accounts
      mount Upmin::Engine => '/admin'

      devise_for :users
      resources :users

      get "console/:id" => "teams#console", as: :console

      get "shortcode/:id" => "teams#shortcode", as: :shortcode

      get "teams/:id/example1" => "teams#example1", as: :example1
      get "teams/:id/example2" => "teams#example2", as: :example2
      get "teams/:id/example3" => "teams#example3", as: :example3
      get "teams/:id/example4" => "teams#example4", as: :example4
      get "teams/:id/example5" => "teams#example5", as: :example5

      post "teams/:id/palettes" => "teams#palettes", as: :palettes  

      authenticated :user do
        root  to: 'teams#home', as: :authenticated
      end

      unauthenticated do
        root  to: 'visitors#index', as: :unauthenticated
      end

      root  to: 'visitors#index'

  end


 


end
