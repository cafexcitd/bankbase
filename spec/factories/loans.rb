FactoryGirl.define do
  factory :loan do
    user_id 1
    amount 1.5
    rate 1.5
    term 1
    repayment 1
  end
end
